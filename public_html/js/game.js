
/* Game namespace */
var game = {

	// an object where to store game information
	data : {
		// score
		score : 0,
                lives: 10,
                coins: 0,
                pausePos: "",
                pausescreen: "",
				ballTimer: 2
	},


	// Run on page load.
	"onload" : function () {
	// Initialize the video.
	if (!me.video.init("screen", 1067, 600, true, 'auto')) {
		alert("Your browser does not support HTML5 canvas.");
		return;
	}

	// add "#debug" to the URL to enable the debug Panel
	if (document.location.hash === "#debug") {
		window.onReady(function () {
			me.plugin.register.defer(this, debugPanel, "debug");
		});
	}

	// Initialize the audio.
	me.audio.init("mp3,ogg");

	// Set a callback to run when loading is complete.
	me.loader.onload = this.loaded.bind(this);

	// Load the resources.
	me.loader.preload(game.resources);

	// Initialize melonJS and display a loading screen.
	me.state.change(me.state.LOADING);
},

	// Run on game resources loaded.
	"loaded" : function () {
		me.state.set(me.state.MENU, new game.TitleScreen());
		me.state.set(me.state.PLAY, new game.PlayScreen());
                me.state.set(me.state.CHARSELECT, new game.CharSelect());
                me.state.set(me.state.GAMEOVER, new game.GameOver());
                //me.state.set(me.state.PREGAME, new game.PreGame());
                //me.state.set(me.state.PAUSE, new game.PauseScreen());

                //me.state.transition("fade", "#FFFFFF", 250);

                 me.pool.register("player", game.PlayerEntity, true);
                 me.pool.register("Pause", game.Pause);
                 me.pool.register("SlimeEntity", game.SlimeEntity, true);
                 me.pool.register("levelTrigger", game.LevelTrigger, true);
                 me.pool.register("otherTrigger", game.OtherTrigger, true);
                 me.pool.register("purpleTrigger", game.PurpleTrigger, true);
                 me.pool.register("redTrigger", game.RedTrigger, true);
                 me.pool.register("yellowTrigger", game.YellowTrigger, true);
                 me.pool.register("greenTrigger", game.GreenTrigger, true);
                 me.pool.register("blueTrigger", game.BlueTrigger, true);
                 me.pool.register("FourDoorTrigger", game.FourDoorTrigger, true);
                 me.pool.register("RedDoorTrigger", game.RedDoorTrigger, true);
                 me.pool.register("YellowDoorTrigger", game.YellowDoorTrigger, true);
                 me.pool.register("GreenDoorTrigger", game.GreenDoorTrigger, true);
                 me.pool.register("BlueDoorTrigger", game.BlueDoorTrigger, true);
                 me.pool.register("FlyEntity", game.FlyEntity, true);
                 me.pool.register("CoinEntity", game.CoinEntity, true);
                 me.pool.register("StarEntity", game.StarEntity, true);
                 me.pool.register("BombEntity", game.BombEntity, true);
                 me.pool.register("BoxEntity", game.BoxEntity, true);
                 me.pool.register("LavaEntity", game.LavaEntity, true);
                 me.pool.register("GeyserEntity", game.GeyserEntity, true);
                 me.pool.register("BlueFlagEntity", game.BlueFlagEntity);
                 me.pool.register("TorchEntity", game.TorchEntity);
                 me.pool.register("BlueTorchEntity", game.BlueTorchEntity);
                 me.pool.register("WaterEntity", game.WaterEntity);
                 me.pool.register("RainbowEntity", game.RainbowEntity);
                 me.pool.register("Rainbow2Entity", game.Rainbow2Entity);
                 me.pool.register("Rainbow3Entity", game.Rainbow3Entity);
                 me.pool.register("Rainbow4Entity", game.Rainbow4Entity);
                 me.pool.register("BrainMonsterEntity", game.BrainMonsterEntity);
                 me.pool.register("PolsVoiceEntity", game.PolsVoiceEntity);
                 me.pool.register("HeartEntity", game.HeartEntity);
                 me.pool.register("HorseheadEntity", game.HorseheadEntity);
                 me.pool.register("GoombasEntity", game.GoombasEntity);
                 me.pool.register("BuzzEntity", game.BuzzEntity);
                 me.pool.register("FrogEntity", game.FrogEntity);
                 me.pool.register("SharkEntity", game.SharkEntity);
                 me.pool.register("SnailEntity", game.SnailEntity);
                 me.pool.register("FinEntity", game.FinEntity);
                 me.pool.register("ShadowEntity", game.ShadowEntity);
                 me.pool.register("WaterslimeEntity", game.WaterslimeEntity);
                 me.pool.register("GreenfishEntity", game.GreenfishEntity);
                 me.pool.register("PinkfishEntity", game.PinkfishEntity);
                 me.pool.register("SplashEntity", game.SplashEntity);
                 me.pool.register("DeathEntity", game.DeathEntity);
                 me.pool.register("SpikesEntity", game.SpikesEntity);
                 me.pool.register("PortalEntity", game.PortalEntity);
                 me.pool.register("SpinnerEntity", game.SpinnerEntity);
                 me.pool.register("LadderEntity", game.LadderEntity);
                 me.pool.register("CharSelect", game.CharSelect);
                 me.pool.register("SwimEntity", game.SwimEntity);
		         me.pool.register("QuickSandEntity", game.QuickSandEntity);
                 me.pool.register("MovingPlatformEntity", game.MovingPlatformEntity);
                 me.pool.register("GhostEntity", game.GhostEntity);
                 me.pool.register("Ghost2Entity", game.Ghost2Entity);
                 me.pool.register("SkeletonEntity", game.SkeletonEntity);
                 me.pool.register("FakeBoxEntity", game.FakeBoxEntity);
                 me.pool.register("FakeDoorEntity", game.FakeDoorEntity);
                 me.pool.register("SeaweedEntity", game.SeaweedEntity);
                 me.pool.register("DarkBlockEntity", game.DarkBlockEntity);
                 me.pool.register("SpringEntity", game.SpringEntity);
                 me.pool.register("BatEntity", game.BatEntity);
                 me.pool.register("VampireBatEntity", game.VampireBatEntity);
                 me.pool.register("BeeEntity", game.BeeEntity);
                 me.pool.register("JellyfishEntity", game.JellyfishEntity);
                 me.pool.register("CharonEntity", game.CharonEntity);
                 me.pool.register("AntiGravity", game.AntiGravity);
                 me.pool.register("FishboneEntity", game.FishboneEntity);
                 me.pool.register("PumpkinEntity", game.PumpkinEntity);
                 me.pool.register("BushEntity", game.BushEntity);
		         me.pool.register("PurpleCoinEntity", game.PurpleCoinEntity);
                 me.pool.register("RedKeyEntity", game.RedKeyEntity);
                 me.pool.register("YellowKeyEntity", game.YellowKeyEntity);
                 me.pool.register("GreenKeyEntity", game.GreenKeyEntity);
                 me.pool.register("BlueKeyEntity", game.BlueKeyEntity);
                 me.pool.register("ScaryEntity", game.ScaryEntity);
                 me.pool.register("PenguinEntity", game.PenguinEntity);
                 me.pool.register("BubbleEntity", game.BubbleEntity);
                 me.pool.register("TallBubbleEntity", game.TallBubbleEntity);
                 me.pool.register("ScaryGhostEntity", game.ScaryGhostEntity);
                 me.pool.register("Cave1Entity", game.Cave1Entity);
                 me.pool.register("Cave2Entity", game.Cave2Entity);
                 me.pool.register("Cave3Entity", game.Cave3Entity);
                 me.pool.register("Cave4Entity", game.Cave4Entity);
                 me.pool.register("Cave5Entity", game.Cave5Entity);
                 me.pool.register("ElectricWaterEntity", game.ElectricWaterEntity);
                 me.pool.register("ExtraLifeEntity", game.ExtraLifeEntity);
                 me.pool.register("fireBall", game.fireBall);
				 me.pool.register("iceBall", game.iceBall);
				 me.pool.register("lifeIcon", game.lifeIcon);
				 me.pool.register("heartIcon", game.heartIcon);
				 me.pool.register("boxIcon", game.boxIcon);
				 me.pool.register("greenTunic", game.greenTunic);
				 me.pool.register("AcidEntity", game.AcidEntity);
				 me.pool.register("MoneyEntity", game.MoneyEntity);
				 me.pool.register("FireRodEntity", game.FireRodEntity);
				 me.pool.register("IceRodEntity", game.IceRodEntity);
				 me.pool.register("BootEntity", game.BootEntity);
				 me.pool.register("BalloonEntity", game.BalloonEntity);
				 me.pool.register("BreakEntity", game.BreakEntity);


                 //MUSIC TRACKS !!!!!!!!!!
                 me.pool.register("JumpEntity", game.JumpEntity);   //Ghost
                 me.pool.register("Jump0Entity", game.Jump0Entity); //Sweet Dreams
                 me.pool.register("Jump1Entity", game.Jump1Entity); //Time
                 me.pool.register("Jump2Entity", game.Jump2Entity);  //mountain
                 me.pool.register("Jump3Entity", game.Jump3Entity);  //underground
                 me.pool.register("Jump4Entity", game.Jump4Entity);  //forest
                 me.pool.register("Jump5Entity", game.Jump5Entity);  //sea
                 me.pool.register("Jump6Entity", game.Jump6Entity);  //dark
                 me.pool.register("Jump7Entity", game.Jump7Entity);  //snow
                 me.pool.register("Jump8Entity", game.Jump8Entity);  //fire
                 me.pool.register("Jump9Entity", game.Jump9Entity);  //desert
                 me.pool.register("Jump10Entity", game.Jump10Entity);//sky
                 me.pool.register("Jump11Entity", game.Jump11Entity);//hell
                 me.pool.register("Jump12Entity", game.Jump12Entity);//space
                 me.pool.register("Jump13Entity", game.Jump13Entity);//pyramid
                 me.pool.register("JumpBonus", game.JumpBonus); //bonus
		         me.pool.register("JumpBonus2", game.JumpBonus2); //entrance
                 me.pool.register("PartyEntity", game.PartyEntity);//party
				 me.pool.register("ShopEntity", game.ShopEntity);

                me.input.bindKey(me.input.KEY.RIGHT, "right");
                me.input.bindKey(me.input.KEY.LEFT, "left");
                me.input.bindKey(me.input.KEY.UP, "jump", true);
                me.input.bindKey(me.input.KEY.DOWN, "down");
                me.input.bindKey(me.input.KEY.R, "resume");
                me.input.bindKey(me.input.KEY.W, "climbup");
                me.input.bindKey(me.input.KEY.Z, "climbdown");
                me.input.bindKey(me.input.KEY.P, "pause", true);
                me.input.bindKey(me.input.KEY.U, "unpause");
                me.input.bindKey(me.input.KEY.X, "attack", true);
				me.input.bindKey(me.input.KEY.I, "shoot", true);

		// Start the game.
		me.state.change(me.state.MENU);
		

	}
};
