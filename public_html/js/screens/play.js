
 /* global game */

game.PlayScreen = me.ScreenObject.extend({
	/**
	 *  action to perform on state change
	 */
	onResetEvent: function() {		// reset the score
            
            me.audio.playTrack("sweet dreams");
		game.data.score = 0;
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
				game.data.tunic = 0;
				game.data.firerod = 0;
				game.data.icerod = 0;
                game.data.aliens = 3;
                game.data.lives = 50;
				game.data.money = 0;
				game.data.hp = 5;
				game.data.boot = 0;
				game.data.balloon = 0;
                me.levelDirector.loadLevel("Level 0");

                //For Level 0: this.resetPlayer(0, 0);
                this.resetPlayer(900, 200);

		// add our HUD to the game world
		this.HUD = new game.HUD.Container();
		me.game.world.addChild(this.HUD);
	},


	/**                this.resetPlayer(900, 200);

	 *  action to perform when leaving this screen (state change)
	 */
	onDestroyEvent: function() {
            me.audio.stopTrack();
		// remove the HUD from the game world
		me.game.world.removeChild(this.HUD);
                me.levelDirector.reloadLevel();
	},

                resetPlayer: function(x, y) {
            var player = me.pool.pull("player", x, y, {});
            me.game.world.addChild(player, 8);

                }
});
