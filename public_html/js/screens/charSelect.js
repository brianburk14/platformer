game.CharSelect = me.ScreenObject.extend({
	/**	
	 *  action to perform on state change
	 */
	onResetEvent: function() {	
            me.game.world.addChild( new me.SpriteObject (0, 0, me.loader.getImage('char_select')), -10);
           // me.input.bindPointer(me.input.mouse.LEFT, "select");
           me.game.world.addChild( new (me.Renderable.extend ({
                        init: function(){
                            me.audio.playTrack("Character_Select");
                            this.paused = false;
                            this.parent(new me.Vector2d(0, 0), 1, 1);
                            this.font = new me.Font("Arial", 38, "white");
                            this.alwaysUpdate = true;
                        },

                        draw: function(context){    
                            this.font.draw(context, "PRESS A, B, C, D, or E TO CHOOSE YOUR HERO", 70, 10);
                        }

                    }))); 
                me.game.world.addChild(new me.SpriteObject (100, 250, me.loader.getImage('p1_front')));
                me.game.world.addChild(new me.SpriteObject (300, 250, me.loader.getImage('p3_front')));
                me.game.world.addChild(new me.SpriteObject (500, 250, me.loader.getImage('p4_front')));
                me.game.world.addChild(new me.SpriteObject (700, 250, me.loader.getImage('p5_front')));
                me.game.world.addChild(new me.SpriteObject (900, 250, me.loader.getImage('p6_front')));

                me.input.bindKey(me.input.KEY.A, "F1", true);
                me.input.bindKey(me.input.KEY.B, "F2", true);
                me.input.bindKey(me.input.KEY.C, "F3", true);
                me.input.bindKey(me.input.KEY.D, "F4", true);
                me.input.bindKey(me.input.KEY.E, "F5", true);         

                this.handler = me.event.subscribe(me.event.KEYDOWN, function (action, keyCode, edge) {
                    me.audio.stopTrack("Character_Select");
                    if(action === "start") {
                        //nothing
                    }
                if (action === "F1") {
                    game.data.character = 1;
                    me.state.change(me.state.PLAY);
                }
                else if(action === "F2"){
                    game.data.character = 2;
                    me.state.change(me.state.PLAY);
                }
                else if(action === "F3"){
                    game.data.character = 3;
                    me.state.change(me.state.PLAY);
                }  
                else if(action === "F4"){
                    game.data.character = 4;
                    me.state.change(me.state.PLAY);
                }  
                else if(action === "F5"){
                    game.data.character = 5;
                    me.state.change(me.state.PLAY);
                }  
                });        
	},
                
	
	/**	
	 *  action to perform when leaving this screen (state change)
	 */
	onDestroyEvent: function() {
		me.input.unbindKey(me.input.KEY.F1);
                me.input.unbindKey(me.input.KEY.F2);
                me.input.unbindKey(me.input.KEY.F3);
                me.input.unbindKey(me.input.KEY.F4);
                me.input.unbindKey(me.input.KEY.F5);
                me.event.unsubscribe(this.handler);
	}
});