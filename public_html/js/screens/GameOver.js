game.GameOver = me.ScreenObject.extend({
	/**	
	 *  action to perform on state change
	 */
	onResetEvent: function() {	
            me.input.bindKey(me.input.KEY.R,"restart");
            me.audio.stopTrack();
            
            var deathImage = new me.SpriteObject(0, 0, me.loader.getImage("gameover"));
            var deathText = new game.MainDeath(1, 1);
            me.game.world.addChild(deathImage, 1);
            me.game.world.addChild(deathText, 2);
	},
	
	
	/**	
	 *  action to perform when leaving this screen (state change)
	 */
        
	onDestroyEvent: function() {
	me.input.unbindKey(me.input.KEY.R);
	}
});

game.MainDeath = me.Renderable.extend ({
    init: function(x, y) {
        this.parent(new me.Vector2d(x, y), 0, 0);
        this.font = new me.Font("Arial", 46, "white");
    },
            
            update: function() {
        if(me.input.isKeyPressed("restart")) {
            me.audio.stopTrack();
            me.state.change(me.state.CHARSELECT);
        }
      
        return true;
            }, 
                    
                 draw: function(context)    {
             this.font.draw(context, "Press 'R' to restart", 250, 530);
             this.font.draw(context, "Copyright 2014", 330, 640);
                 }
});