game.TitleScreen = me.ScreenObject.extend({
	/**	
	 *  action to perform on state change
	 */
	onResetEvent: function() {	
            me.input.bindKey(me.input.KEY.ENTER,"start");
            
            var titleImage = new me.SpriteObject(0, 0, me.loader.getImage("Title-Screen 2"));
            var titleText = new game.MainTitle(1, 1);
            me.audio.playTrack("Title");
            this.paused = false;
            me.game.world.addChild(titleImage, 1);
            me.game.world.addChild(titleText, 2);
	},
	
	
	/**	
	 *  action to perform when leaving this screen (state change)
	 */
        
	onDestroyEvent: function() {
	me.input.unbindKey(me.input.KEY.ENTER);
	}
});

game.MainTitle = me.Renderable.extend ({
    init: function(x, y) {
        this.parent(new me.Vector2d(x, y), 0, 0);
        this.font = new me.Font("Arial", 46, "white");
    },
            
            update: function() {
        if(me.input.isKeyPressed("start")) {
            me.audio.stopTrack();
            me.state.change(me.state.CHARSELECT);
            this.paused = false;
        }
        return true;
            }, 
                    
                 draw: function(context)    {
             this.font.draw(context, "Press 'Enter' to play!", 250, 530);
             this.font.draw(context, "Copyright 2014", 330, 640);
                 }
});