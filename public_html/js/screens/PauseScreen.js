//game.PauseScreen = me.ScreenObject.extend({
//	/**	
//	 *  action to perform on state change
//	 */
//	onResetEvent: function() {	
//            me.input.bindKey(me.input.KEY.P,"pause");
//            this.paused = true;
//            me.audio.pauseTrack();
//            
//            var pauseImage = new me.SpriteObject(0, 0, me.loader.getImage("pause"));
//            var pauseText = new game.MainPause(1, 1);
//            me.game.world.addChild(pauseImage, 1);
//            me.game.world.addChild(pauseText, 2);
//	},
//	
//	
//	/**	
//	 *  action to perform when leaving this screen (state change)
//	 */
//        
//	onDestroyEvent: function() {
//	me.input.unbindKey(me.input.KEY.P);
//	}
//});
//
//game.MainPause = me.Renderable.extend ({
//    init: function(x, y) {
//        this.parent(new me.Vector2d(x, y), 0, 0);
//        this.font = new me.Font("Arial", 46, "white");
//    },
//            
//            update: function() {
//                me.input.bindKey(me.input.KEY.U,"unpause");
//        if(me.input.isKeyPressed("unpause")) {
//            me.audio.resumeTrack();
//            this.paused = false;
//            me.state.change(me.state.PLAY);
//        }
//        return true;
//            }, 
//                    
//                 draw: function(context)    {
//             this.font.draw(context, "Press 'U' to unpause!", 250, 530);
//             this.font.draw(context, "Copyright 2014", 330, 640);
//                 }
//});