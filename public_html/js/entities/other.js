game.lifeIcon = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "life";
        settings.spritewidth = "47";
        settings.spriteheight = "47";
            this.parent(x, y, settings);
     },

	onCollision : function () {
		if(game.data.money >= 66){
			me.audio.play("item");
		game.data.aliens += 1;
		game.data.money -= 66;
		
		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
		} else {
			me.audio.play("error");
		}
	}
});

game.heartIcon = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "heart";
        settings.spritewidth = "88";
        settings.spriteheight = "88";
            this.parent(x, y, settings);
     },

	onCollision : function () {
		if(game.data.money >= 5){
			me.audio.play("item");
		game.data.lives += 10;
		game.data.money -= 5;
		
		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
		} else {
			me.audio.play("error");
		}
	}
});

game.boxIcon = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "background-tiles";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
		this.parent(x, y, settings);
		this.renderable.addAnimation("box", [6,6,7,7,8,8,9,9]);
            this.renderable.setCurrentAnimation("box");
     },

	onCollision : function () {
		if(game.data.money >= 45){
			me.audio.play("item");
		game.data.score += 30;
		game.data.money -= 45;
		
		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
		} else {
			me.audio.play("error");
		}
	}
});

game.greenTunic = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "green tunic";
        settings.spritewidth = "101";
        settings.spriteheight = "70";
            this.parent(x, y, settings);
     },

	onCollision : function () {
		me.audio.play("item");
		game.data.tunic += 1;
		//game.data.money += 1;
                //me.audio.play("coin");

		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
	}
});

game.FireRodEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "firerod";
        settings.spritewidth = "80";
        settings.spriteheight = "74";
            this.parent(x, y, settings);
     },

	onCollision : function () {
		me.audio.play("item");
		game.data.firerod += 1;
		//game.data.money += 1;
                //me.audio.play("coin");

		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
	}
});


game.IceRodEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "icerod";
        settings.spritewidth = "80";
        settings.spriteheight = "70";
            this.parent(x, y, settings);
     },

	onCollision : function () {
		me.audio.play("item");
		game.data.icerod += 1;
		//game.data.money += 1;
                //me.audio.play("coin");

		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
	}
});

game.BootEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "boot";
        settings.spritewidth = "51";
        settings.spriteheight = "36";
            this.parent(x, y, settings);
     },

	onCollision : function () {
		if(game.data.boot == 0){
		me.audio.play("item");
		game.data.boot += 1;
		//game.data.money += 1;
                //me.audio.play("coin");

		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
	}
	}
});

game.BalloonEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "balloon";
        settings.spritewidth = "54";
        settings.spriteheight = "83";
            this.parent(x, y, settings);
			this.setVelocity(3, 0);
			this.vel.x -= this.accel.x * me.timer.tick;
			this.previousVelocity = this.vel.clone();
     },

	onCollision : function () {
		me.audio.play("item");
		game.data.balloon += 1;
		this.gravity = -this.gravity;

		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
	
	},
	
	update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.BreakEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

        this.parent (x, y, settings);

		this.collidable = true;
    },

     onCollision : function (res, obj){
		 if(game.data.balloon == 1){
                game.data.balloon -= 1;
		 }
	}
});