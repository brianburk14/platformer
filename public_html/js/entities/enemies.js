game.SlimeEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "slime-spritesheet";
        settings.spritewidth = "60";
        settings.spriteheight = "28";
        settings.width = 76;
        settings.height = 36;
        this.parent (x, y, settings);

        this.setVelocity(4,10 );

		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;

        this.renderable.addAnimation  ("moving", [1, 2], 300);
		this.renderable.addAnimation ("frozen", [3]);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

	
     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.125;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
			
		} else {
			game.data.lives -= 0.125;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.125;
            } 
			if (res.obj.type === "iceBall") {
			this.setVelocity(0, 0);
			this.renderable.setCurrentAnimation("frozen");
            }
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},

     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }
           this.parent(deltaTime);
           return true;
     }
});

game.WaterslimeEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "waterslime";
        settings.spritewidth = "60";
        settings.spriteheight = "28";
        settings.width = 76;
        settings.height = 36;
        this.parent (x, y, settings);

        this.setVelocity(3,10 );
        // make it collidable
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [1, 2], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.125;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.125;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.125;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},

     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.GoombasEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "goombas";
        settings.spritewidth = "76";
        settings.spriteheight = "70";
        settings.width = 76;
        settings.height = 70;
        this.parent (x, y, settings);

        this.setVelocity(4,10 );
        // make it collidable
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [1, 2], 300);
        this.renderable.addAnimation("smash", [0]);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.5;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.5;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.5;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},

     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.ShadowEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "player2-spritesheet";
        settings.spritewidth = "71";
        settings.spriteheight = "97";
        settings.width = 71;
        settings.height = 97;
        this.parent (x, y, settings);

        this.setVelocity(4, 20.8);
        // make it collidable
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [2, 3, 4, 9, 8, 10], 300);
        this.renderable.addAnimation("jump", [13]);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();

    },

        onCollision : function (res, obj){
var res = me.game.world.collide(this);
                    if(res){
		 if(res.y === "JumpEntity"){
            if(!this.jumping && !this.falling) {
               this.vel.y = -this.maxVel.y * me.timer.tick;
               this.jumping = true;
           }
                 }
             }
		if (this.alive && (res.y > 0)  && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 1;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
                }
	},


     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;


           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.BuzzEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "buzz";
        settings.spritewidth = "70.5";
        settings.spriteheight = "77";
        settings.width = 70.5;
        settings.height = 77;
        this.parent (x, y, settings);
        this.gravity = false;

        this.setVelocity(0 , 4);
        // make it collidable
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0, 1], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "up";

        this.vel.y -= this.accel.y * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.5;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.5;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.5;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},

     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.y === 0) {
           this.vel.y = -this.previousVelocity.y;

           if(this.direction === "up") {
               this.direction = "down";
               this.renderable.flipY(true);
           }
           else {
               this.direction = "down";
               this.renderable.flipY(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.PolsVoiceEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "Pols-Voice";
        settings.spritewidth = "48";
        settings.spriteheight = "48";
        settings.width = 48;
        settings.height = 48;
        this.parent (x, y, settings);
        this.hp = 1;

        this.setVelocity(4,1 );
        // make it collidable
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.5;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.5;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.5;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.BrainMonsterEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "brainmonster";
        settings.spritewidth = "32";
        settings.spriteheight = "64";
        settings.width = 32;
        settings.height = 64;
        this.parent (x, y, settings);

        this.setVelocity(4,10 );
        // make it collidable
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [3, 4, 5], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.5;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.5;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.5;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},

     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.FrogEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "frog";
        settings.spritewidth = "60";
        settings.spriteheight = "56";
        settings.width = 60;
        settings.height = 56;
        this.parent (x, y, settings);

        this.setVelocity(1, 10);
        // make it collidable
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0, 1], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.5;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.5;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.5;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},

     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.SnailEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "snail";
        settings.spritewidth = "55";
        settings.spriteheight = "31";
        settings.width = 55;
        settings.height = 31;
        this.parent (x, y, settings);

        this.setVelocity(1, 10);
        // make it collidable
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0, 1], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.5;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.5;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.5;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},

     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.HorseheadEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "horsehead";
        settings.spritewidth = "100";
        settings.spriteheight = "184";
//        settings.width = 74;
//        settings.height = 136;
        this.parent (x, y, settings);

        this.setVelocity(2,10 );
        // make it collidable
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0, 1], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
                    game.data.lives -= 1;
		}

	},

     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.FlyEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "fly-spritesheet";
        settings.spritewidth = "76";
        settings.spriteheight = "36";
        settings.width = 60;
        settings.height = 28;
        this.parent (x, y, settings);
        this.hp = 1;
        this.setVelocity(4,1 );
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [1, 2], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

    onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.5;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.5;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.5;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.SharkEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "shark";
        settings.spritewidth = "125";
        settings.spriteheight = "57";
        settings.width = 125;
        settings.height = 57;
        this.parent (x, y, settings);
        this.hp = 1;
        this.setVelocity(6, 0);
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.5;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.5;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.5;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.FishboneEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "fishbone";
        settings.spritewidth = "109";
        settings.spriteheight = "70";
        this.parent (x, y, settings);
        this.hp = 1;
        this.setVelocity(4, 0);
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

        onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.25;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.25;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.25;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.GreenfishEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "greenfish";
        settings.spritewidth = "58";
        settings.spriteheight = "45";
        settings.width = 58;
        settings.height = 45;
        this.parent (x, y, settings);
        this.hp = 1;
        this.setVelocity(3.5, 0.5);
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0, 1], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.125;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.125;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.125;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.PinkfishEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "pinkfish";
        settings.spritewidth = "58";
        settings.spriteheight = "45";
        settings.width = 58;
        settings.height = 45;
        this.parent (x, y, settings);
        this.hp = 1;
        this.setVelocity(4.5, -0.5);
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0, 1], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "right";

        this.vel.x += this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.125;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.125;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.125;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "right") {
               this.direction = "left";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "right";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.FinEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "fin";
        settings.spritewidth = "69";
        settings.spriteheight = "89";
        settings.width = 69;
        settings.height = 89;
        this.parent (x, y, settings);
        this.hp = 1;
        this.setVelocity(3, 0);
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){
        if (this.alive && (res.y > 0) && obj.falling) {
if(game.data.boot <= 0){
            game.data.lives -= 0.5;
}
if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
        }
    },
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.SpinnerEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "spinner";
        settings.spritewidth = "62";
        settings.spriteheight = "62";
        settings.width = 62;
        settings.height = 62;
        this.parent (x, y, settings);

		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;

                 this.renderable.addAnimation  ("spin", [0, 1], 300);
        this.renderable.setCurrentAnimation("spin");
    },

     onCollision : function (res, obj){
  if (this.alive && (res.y > 0) && obj.falling) {
            me.audio.play("shawn");
            game.data.lives -= 0.25;
        }
	}
});

game.SkeletonEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "skeleton";
        settings.spritewidth = "106.75";
        settings.spriteheight = "144";
        this.parent (x, y, settings);

        this.setVelocity(5, 20.8);
        // make it collidable
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0, 1, 2, 3], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();

    },

        onCollision : function (res, obj){
		if (this.alive && (res.y > 0)  && obj.falling){
                    game.data.lives -= 0.5;
                }
	},


     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;


           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.GhostEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "ghost";
        settings.spritewidth = "70";
        settings.spriteheight = "71";
        this.parent (x, y, settings);

        this.setVelocity(5, 1);
        // make it collidable
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){
        if (this.alive && (res.y > 0) && obj.falling) {
if(game.data.boot <= 0){
            game.data.lives -= 0.5;
}
if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
        }
    },
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.ScaryGhostEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "scary-ghost";
        settings.spritewidth = "160";
        settings.spriteheight = "162";
        this.parent (x, y, settings);

        this.setVelocity(5, 0);
        // make it collidable
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
                    game.data.lives -= 0.125;
		}
if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}		else {
			game.data.lives -= 0.125;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 1;
			game.data.lives += 0.125;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.Ghost2Entity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "ghost2";
        settings.spritewidth = "51";
        settings.spriteheight = "73";
        this.parent (x, y, settings);

        this.setVelocity(4, 0.5);
        // make it collidable
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){
        if (this.alive && (res.y > 0) && obj.falling) {
if(game.data.boot <= 0){
            game.data.lives -= 0.5;
}
if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
        }
    },
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.BatEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "bat-spritesheet";
        settings.spritewidth = "80";
        settings.spriteheight = "47";
        this.parent (x, y, settings);

        this.setVelocity(5, 1);
        // make it collidable
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("fly", [0, 1], 300);
		this.renderable.addAnimation ("frozen", [2]);
        this.renderable.setCurrentAnimation("fly");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.25;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.25;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.25;
            } 
			if (res.obj.type === "iceBall") {
			this.renderable.setCurrentAnimation("frozen");
			this.setVelocity(0, 0);
            }
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.VampireBatEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "vampirebat-spritesheet";
        settings.spritewidth = "80";
        settings.spriteheight = "47";
        this.parent (x, y, settings);

        this.setVelocity(5, 1);
        // make it collidable
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("fly", [0, 1], 300);
        this.renderable.setCurrentAnimation("fly");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.5;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.5;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.5;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.BeeEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "bee-spritesheet";
        settings.spritewidth = "62";
        settings.spriteheight = "49";
        this.parent (x, y, settings);
        this.hp = 1;
        this.setVelocity(4.5, -0.5);
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0, 1], 300);
		this.renderable.addAnimation ("frozen", [2]);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "right";

        this.vel.x += this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.125;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.125;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.125;
            } 
			if (res.obj.type === "iceBall") {
			this.renderable.setCurrentAnimation("frozen");
			this.setVelocity(0, 0);
            }
			
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "right") {
               this.direction = "left";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "right";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.JellyfishEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "jellyfish";
        settings.spritewidth = "59.5";
        settings.spriteheight = "101";
        this.parent (x, y, settings);
        this.hp = 1;
        this.setVelocity(3, 0);
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0, 1], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "right";

        this.vel.x += this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.125;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.125;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.125;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "right") {
               this.direction = "left";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "right";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.CharonEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "charon";
        settings.spritewidth = "83";
        settings.spriteheight = "151";
        this.parent (x, y, settings);
        this.hp = 1;
        this.setVelocity(3, 0);
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "right";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
                    game.data.lives -= 1;
		} else {
			game.data.lives -= 1;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 1;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "right") {
               this.direction = "left";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "right";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.ScaryEntity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        settings.image = "scary";
        settings.spritewidth = "1089";
        settings.spriteheight = "592";
        this.parent(x, y, settings);
        this.renderable.addAnimation("scary", [0], 80);
            this.renderable.setCurrentAnimation("scary");
             this.type = "ScaryEntity";

    },
});

game.PenguinEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "penguin-spritesheet";
        settings.spritewidth = "112";
        settings.spriteheight = "50";
        this.parent (x, y, settings);
        this.hp = 1;

        this.setVelocity(4,10);
        // make it collidable
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;


        this.renderable.addAnimation  ("moving", [0, 1, 2], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

     onCollision : function (res, obj){

		if (this.alive && (res.y > 0) && obj.falling){
			if(game.data.boot <= 0){
                    game.data.lives -= 0.5;
			}
			if(game.data.boot == 1){
				this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
				me.game.world.removeChild(this);
			}
		} else {
			game.data.lives -= 0.5;
		}
		var res = me.game.world.collide(this);
        if (res) {
        if (res.obj.type === "fireBall") {
			game.data.hp -= 3;
			game.data.lives += 0.5;
            } 
			if(game.data.hp <= 0){
				me.game.world.removeChild(this);
			}
    }
	},
     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});
