
game.LevelTrigger = me.ObjectEntity.extend({
init: function(x, y, settings) {
    this.parent(x, y, settings);
    this.collidable = true;
    this.level = settings.level;
    this.xSpawn = settings.xSpawn;
    this.ySpawn = settings.ySpawn;
},

onCollision: function() {
    this.collidable = false;
    var x = this.xSpawn;
    var y = this.ySpawn;
    me.levelDirector.loadLevel(this.level);
    me.state.current().resetPlayer(x, y);
    me.audio.play("zelda chest");
}
});

game.OtherTrigger = me.ObjectEntity.extend({
init: function(x, y, settings) {
    this.parent(x, y, settings);
    this.collidable = true;
    this.level = settings.level;
    this.xSpawn = settings.xSpawn;
    this.ySpawn = settings.ySpawn;
},

onCollision: function() {
    this.collidable = false;
    var x = this.xSpawn;
    var y = this.ySpawn;
    me.levelDirector.loadLevel(this.level);
    me.state.current().resetPlayer(x, y);
}
});

game.PurpleTrigger = me.ObjectEntity.extend({
init: function(x, y, settings) {
    this.parent(x, y, settings);
    this.collidable = true;
    this.level = settings.level;
    this.xSpawn = settings.xSpawn;
    this.ySpawn = settings.ySpawn;
},

onCollision: function() {
    if(game.data.purple === 100) {
    this.collidable = false;
    var x = this.xSpawn;
    var y = this.ySpawn;
    me.levelDirector.loadLevel(this.level);
    me.state.current().resetPlayer(x, y);
    }
}
});

game.RedTrigger = me.ObjectEntity.extend({
init: function(x, y, settings) {
    this.parent(x, y, settings);
    this.collidable = true;
    this.level = settings.level;
    this.xSpawn = settings.xSpawn;
    this.ySpawn = settings.ySpawn;
},

onCollision: function() {
    if(game.data.red === 1) {
    this.collidable = false;
    var x = this.xSpawn;
    var y = this.ySpawn;
    me.levelDirector.loadLevel(this.level);
    me.state.current().resetPlayer(x, y);
    }
}
});

game.YellowTrigger = me.ObjectEntity.extend({
init: function(x, y, settings) {
    this.parent(x, y, settings);
    this.collidable = true;
    this.level = settings.level;
    this.xSpawn = settings.xSpawn;
    this.ySpawn = settings.ySpawn;
},

onCollision: function() {
    if(game.data.yellow === 1) {
    this.collidable = false;
    var x = this.xSpawn;
    var y = this.ySpawn;
    me.levelDirector.loadLevel(this.level);
    me.state.current().resetPlayer(x, y);
    }
}
});

game.GreenTrigger = me.ObjectEntity.extend({
init: function(x, y, settings) {
    this.parent(x, y, settings);
    this.collidable = true;
    this.level = settings.level;
    this.xSpawn = settings.xSpawn;
    this.ySpawn = settings.ySpawn;
},

onCollision: function() {
    if(game.data.green === 1) {
    this.collidable = false;
    var x = this.xSpawn;
    var y = this.ySpawn;
    me.levelDirector.loadLevel(this.level);
    me.state.current().resetPlayer(x, y);
    }
}
});

game.BlueTrigger = me.ObjectEntity.extend({
init: function(x, y, settings) {
    this.parent(x, y, settings);
    this.collidable = true;
    this.level = settings.level;
    this.xSpawn = settings.xSpawn;
    this.ySpawn = settings.ySpawn;
},

onCollision: function() {
    if(game.data.blue === 1) {
    this.collidable = false;
    var x = this.xSpawn;
    var y = this.ySpawn;
    me.levelDirector.loadLevel(this.level);
    me.state.current().resetPlayer(x, y);
    }
}
});

game.FourDoorTrigger = me.ObjectEntity.extend({
init: function(x, y, settings) {
    this.parent(x, y, settings);
    this.collidable = true;
    this.level = settings.level;
    this.xSpawn = settings.xSpawn;
    this.ySpawn = settings.ySpawn;
},

onCollision: function() {
    if(game.data.blue === 1 && game.data.green === 1 && game.data.yellow === 1 && game.data.red === 1) {
    this.collidable = false;
    var x = this.xSpawn;
    var y = this.ySpawn;
    me.levelDirector.loadLevel(this.level);
    me.state.current().resetPlayer(x, y);
    }
}
});

game.PortalEntity = me.ObjectEntity.extend({
init: function(x, y, settings) {
    settings.image = "portal";
    settings.spritewidth = "147.5";
    settings.spriteheight = "173";
    this.parent(x, y, settings);
    this.renderable.addAnimation("portal", [0, 1, 2, 3]);
            this.renderable.setCurrentAnimation("portal");
    this.collidable = true;
    this.level = settings.level;
    this.xSpawn = settings.xSpawn;
    this.ySpawn = settings.ySpawn;
},

onCollision: function() {
    this.collidable = false;
    var x = this.xSpawn;
    var y = this.ySpawn;
    me.audio.play("portal");
    me.levelDirector.loadLevel(this.level);
    me.state.current().resetPlayer(x, y);
    me.audio.play("zelda chest");
}
});

game.BlueDoorTrigger = me.ObjectEntity.extend({
init: function(x, y, settings) {
    this.parent(x, y, settings);
    this.collidable = true;
    this.level = settings.level;
    this.xSpawn = settings.xSpawn;
    this.ySpawn = settings.ySpawn;
},

onCollision: function() {
    if(game.data.blue === 0) {
    this.collidable = false;
    var x = this.xSpawn;
    var y = this.ySpawn;
    me.levelDirector.loadLevel(this.level);
    me.state.current().resetPlayer(x, y);
    }
}
});

game.GreenDoorTrigger = me.ObjectEntity.extend({
init: function(x, y, settings) {
    this.parent(x, y, settings);
    this.collidable = true;
    this.level = settings.level;
    this.xSpawn = settings.xSpawn;
    this.ySpawn = settings.ySpawn;
},

onCollision: function() {
    if(game.data.green === 0) {
    this.collidable = false;
    var x = this.xSpawn;
    var y = this.ySpawn;
    me.levelDirector.loadLevel(this.level);
    me.state.current().resetPlayer(x, y);
    }
}
});

game.YellowDoorTrigger = me.ObjectEntity.extend({
init: function(x, y, settings) {
    this.parent(x, y, settings);
    this.collidable = true;
    this.level = settings.level;
    this.xSpawn = settings.xSpawn;
    this.ySpawn = settings.ySpawn;
},

onCollision: function() {
    if(game.data.yellow === 0) {
    this.collidable = false;
    var x = this.xSpawn;
    var y = this.ySpawn;
    me.levelDirector.loadLevel(this.level);
    me.state.current().resetPlayer(x, y);
    }
}
});

game.RedDoorTrigger = me.ObjectEntity.extend({
init: function(x, y, settings) {
    this.parent(x, y, settings);
    this.collidable = true;
    this.level = settings.level;
    this.xSpawn = settings.xSpawn;
    this.ySpawn = settings.ySpawn;
},

onCollision: function() {
    if(game.data.red === 0) {
    this.collidable = false;
    var x = this.xSpawn;
    var y = this.ySpawn;
    me.levelDirector.loadLevel(this.level);
    me.state.current().resetPlayer(x, y);
    }
}
});