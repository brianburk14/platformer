game.JumpEntity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "JumpEntity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("Ghost");

    }
});

game.Jump1Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "Jump1Entity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("Time");       
    }
});

game.Jump2Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "Jump2Entity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("mountain");

    }
});

game.Jump3Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "Jump3Entity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("underground");

    }
});

game.Jump4Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "Jump4Entity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("forest");

    }
});

game.Jump5Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "Jump5Entity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("sea");

    }
});

game.Jump6Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "Jump6Entity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("dark");

    }
});

game.Jump7Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "Jump7Entity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("snow");

    }
});

game.Jump8Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "Jump8Entity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("fire");

    }
});

game.Jump9Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "Jump9Entity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("desert");

    }
    });

    game.Jump10Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "Jump10Entity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("sky");

    }
    });

     game.Jump11Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "Jump11Entity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("hell");

    }
    });

     game.Jump12Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "Jump12Entity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("space");

    }
    });

       game.Jump13Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "Jump13Entity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("pyramid");

    }
    });

    game.JumpBonus = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "JumpBonus";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("bonus");

    }
    });

    game.JumpBonus2 = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "JumpBonus2";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("entrance");

    }
    });

    game.PartyEntity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "PartyEntity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("party");
    }
});

game.ShopEntity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        this.parent(x, y, settings);
             this.type = "ShopEntity";
             this.collidable = true;
             me.audio.stopTrack();
             me.audio.playTrack("shop");
    }
});
