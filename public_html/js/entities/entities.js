/* global game */

game.PlayerEntity = me.ObjectEntity.extend({
    init: function(x, y, settings) {

     this.parent(x, y, settings);

     this.collidable = true;

   if(game.data.character === 1){
     settings.image = "player1-spritesheet";
     settings.spritewidth = "72";
     settings.spriteheight = "97";
     settings.width = 72;
     settings.height = 97;
     this.parent(x, y, settings);
     this.renderable.addAnimation("idle", [3]);
     //create a run animation, create an array for values 3 - 11, set the speed
	 //create a jump animation, create an array for value 2
	 //create a climb animation, create an array for values 15 - 16
     this.renderable.addAnimation("jump", [2]);
     this.renderable.addAnimation("run", [3,4,5,6,7,8,9,10,11]);
     this.renderable.addAnimation("climb", [15, 16]);
     this.renderable.addAnimation("duck", [0]);
     this.renderable.addAnimation("hurt", [1]);
     this.renderable.addAnimation("swim", [17, 18]);
	 this.renderable.addAnimation("boot", [12, 13]);
	 this.renderable.addAnimation("balloon", [14]);
     this.renderable.setCurrentAnimation("idle");
       }

   else if(game.data.character === 2){
     settings.image = "player3-spritesheet";
     settings.spritewidth = "72";
     settings.spriteheight = "97";
     settings.width = 72;
     settings.height = 97;
     this.parent(x, y, settings);
     this.renderable.addAnimation("idle", [3]);
     //create a run animation, create an array foor values 4 - 14, set the speed
     this.renderable.addAnimation("jump", [2]);
     this.renderable.addAnimation("run", [3,4,5,6,7,8,9,10,11]);
     this.renderable.addAnimation("climb", [15, 16]);
     this.renderable.addAnimation("duck", [0]);
     this.renderable.addAnimation("hurt", [1]);
     this.renderable.addAnimation("swim", [17, 18]);
	 this.renderable.addAnimation("boot", [12, 13]);
	 this.renderable.addAnimation("balloon", [14]);
     this.renderable.setCurrentAnimation("idle");
       }

   else if(game.data.character === 3){
     settings.image = "player4-spritesheet";
     settings.spritewidth = "72";
     settings.spriteheight = "97";
     settings.width = 72;
     settings.height = 97;
     this.parent(x, y, settings);
     this.renderable.addAnimation("idle", [3]);
     //create a run animation, create an array foor values 4 - 14, set the speed
     this.renderable.addAnimation("jump", [2]);
     this.renderable.addAnimation("run", [3,4,5,6,7,8,9,10,11]);
     this.renderable.addAnimation("climb", [15, 16]);
     this.renderable.addAnimation("duck", [0]);
     this.renderable.addAnimation("hurt", [1]);
     this.renderable.addAnimation("swim", [17, 18]);
	 this.renderable.addAnimation("boot", [12, 13]);
	 this.renderable.addAnimation("balloon", [14]);
     this.renderable.setCurrentAnimation("idle");
       }

   else if(game.data.character === 4){
     settings.image = "player5-spritesheet";
     settings.spritewidth = "71";
     settings.spriteheight = "87";
     settings.width = 70.85;
     settings.height = 87;
     this.parent(x, y, settings);
     this.renderable.addAnimation("idle", [3]);
     this.renderable.addAnimation("jump", [2]);
     this.renderable.addAnimation("run", [3, 4, 5]);
     this.renderable.addAnimation("climb", [6, 7]);
     this.renderable.addAnimation("duck", [0]);
     this.renderable.addAnimation("hurt", [1]);
     this.renderable.addAnimation("swim", [8, 9]);
	 this.renderable.addAnimation("boot", [10, 11]);
	 this.renderable.addAnimation("balloon", [12]);
     this.renderable.setCurrentAnimation("idle");
       }

   else if(game.data.character === 5){
     settings.image = "player6-spritesheet";
     settings.spritewidth = "71.7";
     settings.spriteheight = "96";
     settings.width = 71.7;
     settings.height = 96;
     this.parent(x, y, settings);
     this.renderable.addAnimation("idle", [3]);
     this.renderable.addAnimation("jump", [2]);
     this.renderable.addAnimation("run", [3, 4, 5]);
     this.renderable.addAnimation("climb", [6, 7]);
     this.renderable.addAnimation("duck", [0]);
     this.renderable.addAnimation("hurt", [1]);
     this.renderable.addAnimation("swim", [8, 9]);
	 this.renderable.addAnimation("boot", [11, 12]);
	 this.renderable.addAnimation("balloon", [13]);
     this.renderable.setCurrentAnimation("idle");
       }

     this.setVelocity(5, 23);
     me.game.viewport.follow(this.pos, me.game.viewport.AXIS.BOTH);
    },
    
	setPlayerTimers: function(){
		this.now = new Date().getTime();
		this.lastBall = this.now;
	},
	
    update: function(deltaTime) {
        if(me.input.isKeyPressed("right")){
            this.facing = "right";
            this.renderable.flipX(false);
        this.vel.x += this.accel.x * me.timer.tick;
        }
    else if(me.input.isKeyPressed("left")) {
        this.facing = "left";
        this.renderable.flipX(true);
        this.vel.x -= this.accel.x * me.timer.tick;
    }
    else {
        this.renderable.setCurrentAnimation("run");
        this.renderable.setAnimationFrame();

        this.vel.x = 0;
    }

     if(me.input.isKeyPressed("down")) {
            this.renderable.setCurrentAnimation("duck");
        }
        
        if(me.input.isKeyPressed("attack")){
			if(game.data.firerod >= 1) {
            var fireball = me.pool.pull("fireBall", this.pos.x + 32, this.pos.y + 26, {}, this.facing);
      me.game.world.addChild(fireball, this.z);
      me.audio.play("fireball");
			}
        }

		if(me.input.isKeyPressed("shoot")){
			if(game.data.icerod >= 1) {
            var iceball = me.pool.pull("iceBall", this.pos.x + 32, this.pos.y + 26, {}, this.facing);
      me.game.world.addChild(iceball, this.z);
      me.audio.play("ice");
			}
        }
		
        if(me.input.isKeyPressed("pause") && !this.paused){
                    this.paused = true;
                    me.audio.pauseTrack();
                    me.audio.play("pause");
                    this.pausePos = me.game.viewport.localToWorld(0, 0);
                    game.data.pausePos = me.game.viewport.localToWorld(0, 0);
                    game.data.pausescreen = new me.SpriteObject (game.data.pausePos.x, game.data.pausePos.y, me.loader.getImage('pause'));
                    game.data.pausescreen.updateWhenPaused = true;
                    me.game.world.addChild(game.data.pausescreen, 32);
                    game.data.pauseText = new (me.Renderable.extend ({
                        init: function(){
                            this.parent(new me.Vector2d(game.data.pausePos.x, game.data.pausePos.y), 0, 0);
                            this.font = new me.Font("Arial", 46, "white");
                            this.updateWhenPaused = true;
                            this.alwaysUpdate = true;
                        },

                        draw: function(context){
                            this.font.draw(context, "", (game.data.pausePos.x + 270), (game.data.pausePos.y + 100));
                        }

                    }));
                    me.game.world.addChild(game.data.pauseText, 33);
                    me.state.pause(me.state.PLAY);
                }
                else if(me.input.isKeyPressed("pause")){
                    //nothing
                }
                else if(me.input.isKeyPressed("unpause") && this.paused){
                    me.audio.resumeTrack();
                    this.paused = false;
                    me.state.resume(me.state.PLAY);
                    me.game.world.removeChild(game.data.pausescreen);
                    me.game.world.removeChild(game.data.pauseText);
                }
                
                var level1 = me.game.world.collide(this);
if(level1){
    if(level1.obj.type === "Jump1Entity"){
         if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 1")
                         this.resetPlayer(0, 200);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
    }
}
var level2 = me.game.world.collide(this);
if(level2){
    if(level2.obj.type === "Jump2Entity"){
         if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 2")
                         this.resetPlayer(3570, 800);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
    }
}
var level3 = me.game.world.collide(this);
if(level3){
    if(level3.obj.type === "Jump3Entity"){
         if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 3")
                         this.resetPlayer(100, 2600);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
    }
}
var level4 = me.game.world.collide(this);
if(level4){
    if(level4.obj.type === "Jump4Entity"){
         if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 4")
                         this.resetPlayer(200, 4500);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
    }
}
var level5 = me.game.world.collide(this);
if(level5){
    if(level5.obj.type === "Jump5Entity"){
         if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 5")
                         this.resetPlayer(0, 0);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
    }
}
var level6 = me.game.world.collide(this);
if(level6){
    if(level6.obj.type === "Jump6Entity"){
         if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 6")
                         this.resetPlayer(100, 2800);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
    }
}
var level7 = me.game.world.collide(this);
if(level7){
    if(level7.obj.type === "Jump7Entity"){
         if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 7")
                         this.resetPlayer(4000, 300);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
    }
}
var level8 = me.game.world.collide(this);
if(level8){
    if(level8.obj.type === "Jump8Entity"){
         if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 8")
                         this.resetPlayer(4000, 3300);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
    }
}
var level9 = me.game.world.collide(this);
if(level9){
    if(level9.obj.type === "Jump9Entity"){
         if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 9")
                         this.resetPlayer(4000, 1900);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
    }
}
var level10 = me.game.world.collide(this);
if(level10){
    if(level10.obj.type === "Jump10Entity"){
         if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 10")
                         this.resetPlayer(100, 3300);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
    }
}
var level11 = me.game.world.collide(this);
if(level11){
    if(level11.obj.type === "Jump11Entity"){
         if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 11")
                         this.resetPlayer(100, 200);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
    }
}
var level12 = me.game.world.collide(this);
if(level12){
    if(level12.obj.type === "Jump12Entity"){
         if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 12")
                         this.resetPlayer(0, 220);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
    }
}
var room4 = me.game.world.collide(this);
if(room4){
    if(room4.obj.type === "JumpEntity"){
         if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("room 4")
                         this.resetPlayer(100, 4720);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
    }
}
                
        var water = me.game.world.collide(this);
if(water){
    if(water.obj.type === me.game.WATER_OBJECT){
        if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 5")
                         this.resetPlayer(0, 0);
		game.data.purple = 0;
                game.data.blue = 0;
                game.data.green = 0;
                game.data.yellow = 0;
                game.data.red = 0;
                
                game.data.lives = 100;
             }
            if(me.input.isKeyPressed("jump")) {
                me.audio.play("swim");
             this.vel.y -= this.accel.y * me.timer.tick;
         }
    else if(me.input.isKeyPressed("right")){
            this.facing = "right";
            this.setVelocity(3, 13);
            if(this.jumping || this.falling){
              this.vel.y = (this.gravity)*2.9;
            }
            this.renderable.flipX(false);
        this.vel.x += this.accel.x * me.timer.tick;
        this.renderable.setCurrentAnimation("swim");
        if(me.input.isKeyPressed("jump")) {
                me.audio.play("swim");
             this.vel.y -= this.accel.y * me.timer.tick;
         }
        }
    else if(me.input.isKeyPressed("left")) {
        this.setVelocity(3, 13);
        if(this.jumping || this.falling){
          this.vel.y = (this.gravity)*2.9;
        }
        this.facing = "left";
        this.renderable.flipX(true);
        this.vel.x -= this.accel.x * me.timer.tick;
        this.renderable.setCurrentAnimation("swim");
        if(me.input.isKeyPressed("jump")) {
                me.audio.play("swim");
             this.vel.y -= this.accel.y * me.timer.tick;
         }
    }
    else if(this.falling){
        this.vel.y = (this.gravity)*2.9;
    }
        else {
            this.setVelocity(5, 13);
        }
		this.renderable.setCurrentAnimation("swim");
    }
}

    if(game.data.aliens <= 0) {
                       me.state.change(me.state.GAMEOVER);
                       me.audio.stopTrack();
                       me.audio.play("Game Over");
        }
		
		if(game.data.boot >= 1){
			this.renderable.setCurrentAnimation("boot");
			if (me.input.isKeyPressed("jump")) {
        if (!this.jumping && !this.falling) {
            this.setVelocity(5, 21);
        this.vel.y = -this.maxVel.y * me.timer.tick;
        this.jumping = true;
        me.audio.play("21");
        this.renderable.setCurrentAnimation("boot");

    }
			}
		}
		if(game.data.balloon == 1){
			this.renderable.setCurrentAnimation("balloon");
			this.vel.y = this.gravity*(-3);
			this.falling = false;
			this.gravity = this.gravity*(-1);
			if (me.input.isKeyPressed("jump")) {
				this.renderable.setCurrentAnimation("balloon");
			}
		}

if(this.falling){
    this.setVelocity(5, 21);
}

    if (me.input.isKeyPressed("jump")) {
        if (!this.jumping && !this.falling) {
            this.setVelocity(5, 21);
        this.vel.y = -this.maxVel.y * me.timer.tick;
        this.jumping = true;
        me.audio.play("21");
        this.renderable.setCurrentAnimation("jump");

    } else {
        this.renderable.setCurrentAnimation("run");
    }

        // check & update player movement
        this.updateMovement();

              // check if we fell into a hole
		if (!this.inViewport && (this.pos.y > me.video.getHeight())) {
			// if yes reset the game
			me.game.world.removeChild(this);
			me.game.viewport.fadeIn('#fff', 150, function(){
				me.levelDirector.reloadLevel();
				me.game.viewport.fadeOut('#fff', 150);

			});
			return true;
		}
            }

     // check for collision
    var res = me.game.world.collide(this);
        if (res) {
        // if we collide with an enemy
        if (res.obj.type === me.game.ENEMY_OBJECT && res.obj.type !== "fireBall") {
            // check if we jumped on it
            if ((res.y > 0) && ! this.jumping) {
				if(game.data.boot <= 0){
                // bounce (force jump)
                this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
                this.renderable.setCurrentAnimation("hurt");
//                this.renderable.flicker(750);
                me.audio.play("shawn");
                // set the jumping flag
                this.jumping = true;
				}

            } else {
                // let's flicker in case we touched an enemy
                this.renderable.setCurrentAnimation("hurt");
                this.falling = false;
                this.renderable.flicker(750);
                me.audio.play("shawn");
				if(game.data.boot == 1){
				game.data.boot -= 1;
				}
            }

        }

    }

     var ladder = me.game.world.collide(this);
        if(ladder){
        if (ladder.obj.type === "LadderEntity") {
        if(me.input.isKeyPressed("climbup")){
            this.renderable.setCurrentAnimation("climb");
            this.setVelocity(0, 5);
            this.vel.y -= this.accel.y * me.timer.tick;
            this.jumping = false;
        } else if (me.input.isKeyPressed("climbdown")){
            this.renderable.setCurrentAnimation("climb");
            this.setVelocity(0, 5);
            this.vel.y += this.accel.y * me.timer.tick;
            this.jumping = false;
        } else {
            this.renderable.setCurrentAnimation("climb");
            this.renderable.setAnimationFrame();
        this.vel.y = 0;
    }
        }
    }

    var antigravity = me.game.world.collide(this);
        if(antigravity){
        if (antigravity.obj.type === "AntiGravity") {
            this.gravity = -this.gravity;
            this.renderable.flipY(true);
             if (me.input.isKeyPressed("jump")) {
        if (!this.jumping && !this.falling) {
        this.vel.y = this.maxVel.y * me.timer.tick;
        this.jumping = true;
        me.audio.play("21");
        this.renderable.setCurrentAnimation("jump");

    }}
        } else {
        this.gravity = true;
        this.renderable.flipY(false);
    }
    }


    var collision = me.game.world.collide(this);
    this.parent(deltaTime);
    this.updateMovement();
    this.lastPause = new Date().getTime();
    this.updateWhenPaused = true;
    this.pausePos = 0, 0;
    return true;
},

resetPlayer: function(x, y) {
            var player = me.pool.pull("player", x, y, {});
            me.game.world.addChild(player, 8);
                }

});

game.CoinEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "item-spritesheet";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
            this.parent(x, y, settings);
            this.renderable.addAnimation("coin", [13,14,15]);
            this.renderable.setCurrentAnimation("coin");
     },

	onCollision : function () {
		game.data.score += 1;
		game.data.money += 1;
                me.audio.play("coin");

		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
	}
});


game.MoneyEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "money";
        settings.spritewidth = "80";
        settings.spriteheight = "80";
            this.parent(x, y, settings);
     },

	onCollision : function () {
		game.data.score += 3;
		game.data.money += 10;
                me.audio.play("money");

		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
	}
});


game.fireBall = me.ObjectEntity.extend({
   init: function (x, y, settings, facing) {
       settings.spritewidth = "48";
       settings.spriteheight = "48";
	   settings.width = 48;
	   settings.height  = 48;
       settings.image = "fireball";
       this.parent(x, y, settings);
       
       this.setVelocity (10, 0);
       this.collidable = true;
       this.alwaysUpdate = true;
	   this.stuck = false;
       
       this.type = "fireBall";
       this.facing = facing;
	   this.attacking = true;
	   
   },
   
   update: function(deltaTime) {
       if(this.facing === "right"){
           this.vel.x += this.accel.x * me.timer.tick;
       }
       else if(this.facing === "left"){
		   this.renderable.flipX(true);
           this.vel.x -= this.accel.x * me.timer.tick;
       }
	   this.updateMovement();
       
       //This is the CollideHandler
       var fire = me.game.world.collide(this);
        if (fire) {
        // if we collide with an enemy
        if (fire.obj.type === me.game.ENEMY_OBJECT) {
            me.game.world.removeChild(this);
            }
    }
	var bar = me.game.world.collide(this);
        if (bar) {
        // if we collide with an enemy
        if (bar.obj.type === this.type.SOLID) {
            me.game.world.removeChild(this);
            }
    }
   }
});

game.iceBall = me.ObjectEntity.extend({
   init: function (x, y, settings, facing) {
       settings.spritewidth = "48";
       settings.spriteheight = "48";
	   settings.width = 48;
	   settings.height  = 48;
       settings.image = "iceball";
       this.parent(x, y, settings);
       
       this.setVelocity (10, 0);
       this.collidable = true;
       this.alwaysUpdate = true;
	   this.stuck = false;
       
       this.type = "iceBall";
       this.facing = facing;
	   this.attacking = true;
	   
   },
   
   update: function(deltaTime) {
       if(this.facing === "right"){
           this.vel.x += this.accel.x * me.timer.tick;
       }
       else if(this.facing === "left"){
		   this.renderable.flipX(true);
           this.vel.x -= this.accel.x * me.timer.tick;
       }
	   this.updateMovement();
       
       //This is the CollideHandler
       var ice = me.game.world.collide(this);
        if (ice) {
        // if we collide with an enemy
        if (ice.obj.type === me.game.ENEMY_OBJECT) {
            me.game.world.removeChild(this);
            }
    }
	var bar = me.game.world.collide(this);
        if (bar) {
        // if we collide with an enemy
        if (bar.obj.type === this.type.SOLID) {
            me.game.world.removeChild(this);
            }
    }
   }
});

game.PurpleCoinEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "purple-coin";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
            this.parent(x, y, settings);
            this.renderable.addAnimation("coin", [0, 1, 2]);
            this.renderable.setCurrentAnimation("coin");
     },

	onCollision : function () {
		game.data.purple += 1;
                me.audio.play("purple-coin");

		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
	}
});

game.RedKeyEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "item-spritesheet";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
            this.parent(x, y, settings);
            this.renderable.addAnimation("key", [35]);
            this.renderable.setCurrentAnimation("key");
     },

	onCollision : function () {
		game.data.red += 1;
                me.audio.play("key");

		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
	}
});

game.YellowKeyEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "item-spritesheet";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
            this.parent(x, y, settings);
            this.renderable.addAnimation("key", [36]);
            this.renderable.setCurrentAnimation("key");
     },

	onCollision : function () {
		game.data.yellow += 1;
                me.audio.play("key");

		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
	}
});

game.GreenKeyEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "item-spritesheet";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
            this.parent(x, y, settings);
            this.renderable.addAnimation("key", [34]);
            this.renderable.setCurrentAnimation("key");
     },

	onCollision : function () {
		game.data.green += 1;
                me.audio.play("key");

		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
	}
});

game.BlueKeyEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "item-spritesheet";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
            this.parent(x, y, settings);
            this.renderable.addAnimation("key", [33]);
            this.renderable.setCurrentAnimation("key");
     },

	onCollision : function () {
		game.data.blue += 1;
                me.audio.play("key");

		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
	}
});

game.HeartEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "heart";
        settings.spritewidth = "88";
        settings.spriteheight = "88";
            this.parent(x, y, settings);
            this.renderable.addAnimation("heart", [0]);
            this.renderable.setCurrentAnimation("heart");
     },

	onCollision : function () {
		game.data.lives += 5;
                me.audio.play("heart");

		//avoid further collision and delete it
		this.collidable = false;
		me.game.world.removeChild(this);
	}
});

game.SpringEntity = me.ObjectEntity.extend({
     init: function (x, y, settings){
   settings.image = "item-spritesheet";
   settings.spritewidth = "70";
        settings.spriteheight = "70";
        this.type = me.game.ENEMY_OBJECT;
            this.parent(x, y, settings);
            this.renderable.addAnimation("normal",[44]);
            this.renderable.addAnimation("boing", [45]);
            this.renderable.setCurrentAnimation("normal");
     }
});

game.BlueFlagEntity = me.ObjectEntity.extend({
   init: function (x, y, settings){
       settings.image = "item-spritesheet";
       settings.spritewidth = "70";
        settings.spriteheight = "70";
            this.parent(x, y, settings);
            this.renderable.addAnimation("flag", [17, 18]);
            this.renderable.setCurrentAnimation("flag");
   }
});

game.TorchEntity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        settings.image = "background-tiles";
       settings.spritewidth = "70";
        settings.spriteheight = "70";
        this.parent(x, y, settings);
        this.renderable.addAnimation("torch", [149, 150]);
            this.renderable.setCurrentAnimation("torch");
    }
});

game.BlueTorchEntity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        settings.image = "background-tiles-water";
       settings.spritewidth = "70";
        settings.spriteheight = "70";
        this.parent(x, y, settings);
        this.renderable.addAnimation("bluetorch", [149, 150]);
            this.renderable.setCurrentAnimation("bluetorch");
    }
});

game.BushEntity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        settings.image = "bush";
       settings.spritewidth = "216";
        settings.spriteheight = "72";
        this.parent(x, y, settings);
        this.renderable.addAnimation("bush", [0]);
            this.renderable.setCurrentAnimation("bush");
    }
});

game.WaterEntity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        settings.image = "background-tiles-water";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
        this.parent(x, y, settings);
        this.renderable.addAnimation("water", [84, 85]);
            this.renderable.setCurrentAnimation("water");
    }
});

game.RainbowEntity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        settings.image = "rainbow-tiles";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
        this.parent(x, y, settings);
        this.renderable.addAnimation("rainbow", [0, 1, 2]);
            this.renderable.setCurrentAnimation("rainbow");
             this.type = "RainbowEntity";

    },
});

game.Rainbow2Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        settings.image = "rainbow-tiles";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
        this.parent(x, y, settings);
        this.renderable.addAnimation("rainbow", [2, 3, 4]);
            this.renderable.setCurrentAnimation("rainbow");
             this.type = "Rainbow2Entity";

    },
});

game.Rainbow3Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        settings.image = "rainbow-tiles";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
        this.parent(x, y, settings);
        this.renderable.addAnimation("rainbow", [4, 5, 6]);
            this.renderable.setCurrentAnimation("rainbow");
             this.type = "Rainbow3Entity";

    },
});

game.Rainbow4Entity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        settings.image = "rainbow-tiles";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
        this.parent(x, y, settings);
        this.renderable.addAnimation("rainbow", [5, 6, 7]);
            this.renderable.setCurrentAnimation("rainbow");
             this.type = "Rainbow4Entity";

    },
});

game.PumpkinEntity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        settings.image = "pumpkin";
        settings.spritewidth = "70";
        settings.spriteheight = "76";
        this.parent(x, y, settings);
        this.renderable.addAnimation("pumpkin", [0]);
            this.renderable.setCurrentAnimation("pumpkin");

    },
});

game.DarkBlockEntity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        settings.image = "background-tiles-water";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
        this.parent(x, y, settings);
        this.renderable.addAnimation("block", [2, 3]);
            this.renderable.setCurrentAnimation("block");

    },
});

game.SeaweedEntity = me.ObjectEntity.extend({
    init: function (x, y, settings){
        settings.image = "realseaweed";
        settings.spritewidth = "62";
        settings.spriteheight = "86";
        this.parent(x, y, settings);
                this.type = me.game.WATER_OBJECT;
        this.renderable.addAnimation("seaweed", [0, 1]);
            this.renderable.setCurrentAnimation("seaweed");
             this.type = "SeaweedEntity";

    },
});

game.LavaEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

        this.parent (x, y, settings);

		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;
    },

     onCollision : function (res, obj){
         me.audio.play("lava");
                game.data.lives -= 100;
	}
});

game.ElectricWaterEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

       settings.image = "background-tiles-water";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
        this.parent (x, y, settings);

		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;

                this.renderable.addAnimation  ("electric", [81, 82], 300);
        this.renderable.setCurrentAnimation("electric");
    },

     onCollision : function (res, obj){
         me.audio.play("shawn");
                game.data.lives -= 5;
	}
});

game.GeyserEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

        this.parent (x, y, settings);

		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;
    },

     onCollision : function (res, obj){
         me.audio.play("splash");
                game.data.lives -= 100;
	}
});

game.SpikesEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "item-spritesheet";
        settings.spritewidth = "70.5";
        settings.spriteheight = "77";
        settings.width = 70.5;
        settings.height = 77;
        this.parent (x, y, settings);

		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;

                this.renderable.addAnimation  ("spike", [43], 300);
        this.renderable.setCurrentAnimation("spike");
    },

     onCollision : function (res, obj){

                game.data.lives -= 5;
	}
});

game.DeathEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

        this.parent (x, y, settings);

		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;
    },

     onCollision : function (res, obj){
         game.data.lives -= 100;
	}
});

game.SwimEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

        this.parent (x, y, settings);
                this.type = me.game.WATER_OBJECT;
    },

     onCollision : function (res, obj){

	}
});

game.AcidEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

        this.parent (x, y, settings);
                this.type = me.game.WATER_OBJECT;
    },

     onCollision : function (res, obj){
		 if(game.data.tunic >= 1) {
		//swim
		if(game.data.lives <= 0) {
             game.data.aliens -= 1;
                 me.levelDirector.loadLevel("Level 8")
                         this.resetPlayer(4000, 3200);
             }
	 } 
	else {
		game.data.lives -= 50;
		me.audio.play("lava");
	}
	 }
}); 

game.QuickSandEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

        this.parent (x, y, settings);
                this.type = "QuickSandEntity";
    },

     onCollision : function (res, obj){

	}
});

game.SplashEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

        this.parent (x, y, settings);

		this.collidable = true;

    },

     onCollision : function (res, obj){
         me.audio.play("water");
         this.collidable = false;
	}
});

game.LadderEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
    this.parent (x, y, settings);
    this.collidable = true;
    this.type = "LadderEntity";
    },

     onCollision : function (){

	}
});

game.AntiGravity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
    this.parent (x, y, settings);
    this.collidable = true;
    this.type = "AntiGravity";
    },

     onCollision : function (){

	}
});

game.StarEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	    settings.image = "item-spritesheet";
            settings.spritewidth = "70";
            settings.spriteheight = "70";
            this.parent(x, y, settings);
            this.renderable.addAnimation("star", [46]);
            this.renderable.setCurrentAnimation("star");
     },
	onCollision : function () {
		game.data.score += 5;
                me.audio.play("pickup");
		this.collidable = false;
		me.game.world.removeChild(this);
	}
});

game.ExtraLifeEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	    settings.image = "life";
            settings.spritewidth = "47";
            settings.spriteheight = "47";
            this.parent(x, y, settings);
            this.renderable.addAnimation("life", [0]);
            this.renderable.setCurrentAnimation("life");
     },
	onCollision : function () {
		game.data.aliens += 1;
                me.audio.play("bonuslife");
		this.collidable = false;
		me.game.world.removeChild(this);
	}
});

game.BoxEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "background-tiles";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
            this.parent(x, y, settings);
            this.renderable.addAnimation("box", [6,6,7,7,8,8,9,9]);
            this.renderable.setCurrentAnimation("box");
     },
	onCollision : function () {
                game.data.score += 5;
		this.collidable = true;
                me.audio.play("level finish");
                me.game.world.removeChild(this);
	}
});

game.FakeBoxEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "background-tiles-water";
        settings.spritewidth = "70";
        settings.spriteheight = "70";
            this.parent(x, y, settings);
            this.renderable.addAnimation("fakebox", [6,6,7,7,8,8,9,9]);
            this.renderable.setCurrentAnimation("fakebox");
     },
	onCollision : function () {
		this.collidable = true;
                me.audio.play("laugh");
                me.game.world.removeChild(this);
	}
});

game.FakeDoorEntity = me.CollectableEntity.extend({
	init: function (x, y, settings) {
	 settings.image = "FakeDoor";
        settings.spritewidth = "70";
        settings.spriteheight = "110";
            this.parent(x, y, settings);
     },
	onCollision : function () {
		this.collidable = true;
                me.audio.play("laugh");
                me.game.world.removeChild(this);
	}
});

game.Cave1Entity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

        this.parent (x, y, settings);
    },

     onCollision : function (res, obj){
   me.audio.play("cave1");
   this.collidable = false;
	}
});

game.Cave2Entity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

        this.parent (x, y, settings);
        this.type = me.game.WATER_OBJECT;
    },

     onCollision : function (res, obj){
   me.audio.play("cave2");
   this.collidable = false;
	}
});

game.Cave3Entity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

        this.parent (x, y, settings);
    },

     onCollision : function (res, obj){
   me.audio.play("cave3");
   this.collidable = false;
	}
});

game.Cave4Entity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

        this.parent (x, y, settings);
    },

     onCollision : function (res, obj){
   me.audio.play("cave4");
   this.collidable = false;
	}
});

game.Cave5Entity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {

        this.parent (x, y, settings);
    },

     onCollision : function (res, obj){
   me.audio.play("cave5");
   this.collidable = false;
	}
});

game.BubbleEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "bubble";
        settings.spritewidth = "141";
        settings.spriteheight = "138";
        this.parent (x, y, settings);
        this.setVelocity(2, 0);
        this.type = me.game.WATER_OBJECT;

        this.renderable.addAnimation  ("moving", [0], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.x -= this.accel.x * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

    onCollision : function (res, obj){

            },

     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.x = -this.previousVelocity.x;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipX(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipX(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});

game.TallBubbleEntity = me.ObjectEntity.extend ({
    init: function (x, y, settings) {
        settings.image = "tall-bubble";
        settings.spritewidth = "113";
        settings.spriteheight = "259";
        this.parent (x, y, settings);
        this.setVelocity(0, 0);
        this.type = me.game.WATER_OBJECT;

        this.renderable.addAnimation  ("moving", [0], 300);
        this.renderable.setCurrentAnimation("moving");
        this.direction = "left";

        this.vel.y -= this.accel.y * me.timer.tick;
        this.previousVelocity = this.vel.clone();
    },

    onCollision : function (res, obj){

            },

     update: function(deltaTime) {
        var collision = this.updateMovement ();

        if(collision && this.vel.x === 0) {
           this.vel.y = -this.previousVelocity.y;

           if(this.direction === "left") {
               this.direction = "right";
               this.renderable.flipY(true);
           }
           else {
               this.direction = "left";
               this.renderable.flipY(false);
           }
        }
           else {
               this.previousVelocity = this.vel.clone ();
           }

           this.parent(deltaTime);
           return true;
     }
});


game.ResetEntity = me.ObjectEntity.extend({

    init: function() {
		var self = this;
		this.player = me.game.getEntityByName("player")[0];
		var x = this.player.pos.x;
		var y = this.player.pos.y;
		self.parent(x, y, {image: "chargib", spritewidth: 32});
		self.init = true;

		self.gravity = 0;
		self.animationspeed = 4;

		this.pos.x = x;
		this.pos.y = y;
	},

	update: function() {
		var self = this;

		if(self.init) {
			setTimeout(function() {
				me.game.remove(self);
			}, 500);
			self.init = false;
		}

		this.parent(this);

		return true;
	}
});

