

/**
 * a HUD container and child items
 */

game.HUD = game.HUD || {};


game.HUD.Container = me.ObjectContainer.extend({
    

	init: function() {
		// call the constructor
		this.parent();

		// persistent across level change
		this.isPersistent = true;

		// non collidable
		this.collidable = false;

		// make sure our object is always draw first
		this.z = Infinity;

		// give a name
		this.name = "HUD";

		// add our child score object at the top left corner
		this.addChild(new game.HUD.ScoreItem(5, 5));
                this.addChild(new game.HUD.LivesItem(5, 5));
                this.addChild(new game.HUD.CoinsItem(5, 5));
                this.addChild(new game.HUD.StarItem(5, 5));
		this.addChild(new game.HUD.PurpleItem(5, 5));
                this.addChild(new game.HUD.Aliens(5, 5));
				this.addChild(new game.HUD.Money(5, 5));
                                                                
	}
});

/**
 * a basic HUD item to display score
 */
game.HUD.ScoreItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "green");
		// local copy of the global score
		this.score = -1;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the score has been updated
		if (this.score !== game.data.score) {
			this.score = game.data.score;
			return true;
		}
		return false;
	},

	/**
	 * draw the score
	 */
	draw : function (context) {
// draw it baby !
    this.font.draw(context, "Score:" + this.score, 400, 0);
}
});

game.HUD.PurpleItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "purple");
		// local copy of the global score
		this.purple = -1;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the score has been updated
		if (this.purple !== game.data.purple) {
			this.purple = game.data.purple;
			return true;
		}
		return false;
	},

	/**
	 * draw the score
	 */
	draw : function (context) {
// draw it baby !
    this.font.draw(context, "Purple Coins:" + this.purple + "/100", 700, 0);
}
});

game.HUD.Aliens = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "white");
		// local copy of the global score
		this.aliens = -1;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the score has been updated
		if (this.aliens !== game.data.aliens) {
			this.aliens = game.data.aliens;
			return true;
		}
		return false;
	},

	/**
	 * draw the score
	 */
	draw : function (context) {
    this.font.draw(context, " Lives: " + this.aliens, 50, 550);
}
});

game.HUD.RedItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "");
		// local copy of the global score
		this.red = -1;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the score has been updated
		if (this.red !== game.data.red) {
			this.red = game.data.red;
			return true;
		}
		return false;
	},

	/**
	 * draw the score
	 */
	draw : function (context) {
// draw it baby !
    this.font.draw(context, "" + this.red + "", 700, 0);
}
});

game.HUD.YellowItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "");
		// local copy of the global score
		this.yellow = -1;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the score has been updated
		if (this.yellow !== game.data.yellow) {
			this.yellow = game.data.yellow;
			return true;
		}
		return false;
	},

	/**
	 * draw the score
	 */
	draw : function (context) {
// draw it baby !
    this.font.draw(context, "" + this.yellow + "", 700, 0);
}
});

game.HUD.GreenItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "");
		// local copy of the global score
		this.green = -1;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the score has been updated
		if (this.green !== game.data.green) {
			this.green = game.data.green;
			return true;
		}
		return false;
	},

	/**
	 * draw the score
	 */
	draw : function (context) {
// draw it baby !
    this.font.draw(context, "" + this.green + "", 700, 0);
}
});

game.HUD.BlueItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "");
		// local copy of the global score
		this.blue = -1;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the score has been updated
		if (this.blue !== game.data.blue) {
			this.blue = game.data.blue;
			return true;
		}
		return false;
	},

	/**
	 * draw the score
	 */
	draw : function (context) {
// draw it baby !
    this.font.draw(context, "" + this.blue + "", 700, 0);
}
});

game.HUD.CoinsItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "blue");
		// local copy of the global score

		// make sure we use screen coordinates
		this.floating = true;
	},
	/**
	 * draw the score
	 */
	draw : function (context) {
// draw it baby !
    this.font.draw(context, "Coins + 1pt", 10, 0);
}
});

game.HUD.StarItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "yellow");
		// local copy of the global score

		// make sure we use screen coordinates
		this.floating = true;
	},
	/**
	 * draw the score
	 */
	draw : function (context) {
// draw it baby !
    this.font.draw(context, "Stars + 5pts", 10, 30);
}
});

game.HUD.Money = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "orange");
		// local copy of the global score
		this.money = -1;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the score has been updated
		if (this.money !== game.data.money) {
			this.money = game.data.money;
			return true;
		}
		return false;
	},

	/**
	 * draw the score
	 */
	draw : function (context) {
// draw it baby !
    this.font.draw(context, "$" + this.money, 700, 40);
}
});

game.HUD.LivesItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "red");
		// local copy of the global lives
		this.lives = 0;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the lives has been updated
		if (this.lives !== game.data.lives) {
			this.lives = game.data.lives;
			return true;
		}
		return false;
	},

	/**
	 * draw the lives
	 */
	draw : function (context) {
// draw it baby !
    this.font.draw(context, "Health:" + this.lives, 400, 40);
	if(game.data.lives <= 0){
		this.font.draw(context, "Health:" + "0", 400, 40);
	}
}
});

game.HUD.TunicItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "red");
		// local copy of the global lives
		this.tunic = 0;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the lives has been updated
		if (this.tunic !== game.data.tunic) {
			this.tunic = game.data.tunic;
			return true;
		}
		return false;
	},

	/**
	 * draw the lives
	 */
});

game.HUD.FireRodItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "red");
		// local copy of the global lives
		this.firerod = 0;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the lives has been updated
		if (this.firerod !== game.data.firerod) {
			this.firerod = game.data.firerod;
			return true;
		}
		return false;
	},

	/**
	 * draw the lives
	 */
});

game.HUD.IceRodItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "red");
		// local copy of the global lives
		this.icerod = 0;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the lives has been updated
		if (this.icerod !== game.data.icerod) {
			this.icerod = game.data.icerod;
			return true;
		}
		return false;
	},

	/**
	 * draw the lives
	 */
});


game.HUD.hpItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "red");
		// local copy of the global lives
		this.hp = 0;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the lives has been updated
		if (this.hp !== game.data.hp) {
			this.hp = game.data.hp;
			return true;
		}
		return false;
	},

	/**
	 * draw the lives
	 */
});

game.HUD.BootItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "red");
		// local copy of the global lives
		this.boot = 0;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the lives has been updated
		if (this.boot !== game.data.boot) {
			this.boot = game.data.boot;
			return true;
		}
		return false;
	},

	/**
	 * draw the lives
	 */
});

game.HUD.BalloonItem = me.Renderable.extend({
	/**
	 * constructor
	 */
	init: function(x, y) {

		// call the parent constructor
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);

		this.font = new me.Font("Arial", 35, "red");
		// local copy of the global lives
		this.balloon = 0;

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
		// we don't do anything fancy here, so just
		// return true if the lives has been updated
		if (this.balloon !== game.data.balloon) {
			this.balloon = game.data.balloon;
			return true;
		}
		return false;
	},

	/**
	 * draw the lives
	 */
});